from django.shortcuts import render

# Create your views here.

from .models import Post

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt

def post_list(request):
    posts = Post.objects.all()
    return render(request, 'posts/post_list.html', {'posts': posts})
